const process = require('node:process');
const { spawn } = require('node:child_process');

module.exports = (name) => {
  const date = new Date().toISOString();

  const cp =  spawn('ffmpeg', [
    '-rtbufsize',
    '1500M',
    '-f',
    'x11grab',
    '-framerate',
    '25',
    '-i',
    ':1',
    `/recordings/${name}-${date}.mp4`
  ], { env: process.env, stdio: 'inherit' });

  cp.on('error', console.log);
  return cp;
}