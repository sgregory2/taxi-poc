const process = require('node:process');
const { spawn } = require('child_process');

module.exports = function createWebdriverProcess(config) {
  return spawn('chromedriver', [`--host=${config.host}`, `--port=${config.port}`], { env: process.env });
}