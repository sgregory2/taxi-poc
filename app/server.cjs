const process = require('node:process');
const axios = require('axios');
const fastify = require('fastify')({ logger: true });
const path = require('path');
const ffmpeg = require('./ffmpeg.cjs')
const createWebdriverProcess = require('./webdriver.cjs');

const WEBDRIVER_HOST = process.env.WEBDRIVER_HOST || 'localhost'
const WEBDRIVER_PORT = process.env.WEBDRIVER_PORT || 4444
const WEBDRIVER_URL = `http://${WEBDRIVER_HOST}:${WEBDRIVER_PORT}`;

const stripLocalPath = (path) => {
  return path.replace(/^\/wd\/hub\//, '');
}

axios.defaults.validateStatus = () => true;

const request = axios.create({
  baseURL: WEBDRIVER_URL,
  timeout: 60000,
  headers: { 'Content-Type': 'application/json', 'Connection': 'Keep-Alive', 'Accept-Encoding': 'gzip' }
})

const proxyRequest = async (req, rep) => {
  const body = req.body;
  const method = req.method;
  const path = stripLocalPath(req.url);
  const res = await request.request({
    url: path,
    data: body,
    method: method
  });

  rep
    .status(res.status)
    .send(res.data);
}

fastify.register(require('@fastify/static'), {
  root: path.join(__dirname, '..', 'dist')
});

fastify.register(require('@fastify/websocket'))

const sockets = [];
const driver = createWebdriverProcess({
  host: WEBDRIVER_HOST,
  port: WEBDRIVER_PORT
})

const disconnectRecording = (child) => {
  if (child) {
    child.kill("SIGTERM");
    child = null;
  } else {
    console.log('recording not started');
  }
}

var recordingProcess = null;

fastify.get('/window', { websocket: true }, (conn, req) => {
  sockets.push(conn.socket);
});

fastify.post('/wd/hub/session', async(req, reply) => {
  const caps = req.body.capabilities.alwaysMatch
  const testName = `${(caps.testName ?? 'vid').replaceAll(' ', '_')}-${caps.browserName}`
  recordingProcess = await ffmpeg(testName)

  await proxyRequest(req, reply)
})

fastify.delete('/wd/hub/session/:sessionId', async(req, reply) => {
  disconnectRecording(recordingProcess);
  await proxyRequest(req, reply);
})

fastify.get('/wd/hub/*', async(req, reply) => {
  await proxyRequest(req, reply)
})

fastify.post('/wd/hub/*', async(req, reply) => {
  await proxyRequest(req, reply);
});

fastify.delete('/wd/hub/*', async(req, reply) => {
  await proxyRequest(req, reply);
})

fastify.put('/wd/hub/*', async(req, reply) => {
  await proxyRequest(req, reply);
})
const start = async() => {
  try {
    await fastify.listen({ port: 3000, host: '0.0.0.0' });
  } catch(err) {
    fastify.log.error(err);
    driver.abort();
    disconnectRecording(recordingProcess);
    process.exit(1);
  }
}

process.on('exit', () => {
  driver.abort();
  disconnectRecording(recordingProcess);
})

start();