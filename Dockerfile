FROM node:18-bullseye

RUN apt update -y && apt install -y \
    novnc \
    x11vnc \
    xvfb \
    wget \
    unzip \
    golang \
    fluxbox \
    ffmpeg \
    vim

# Install Chrome WebDriver
RUN CHROMEDRIVER_VERSION=`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE` && \
    mkdir -p /opt/chromedriver-$CHROMEDRIVER_VERSION && \
    curl -sS -o /tmp/chromedriver_linux64.zip http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip && \
    unzip -qq /tmp/chromedriver_linux64.zip -d /opt/chromedriver-$CHROMEDRIVER_VERSION && \
    rm /tmp/chromedriver_linux64.zip && \
    chmod +x /opt/chromedriver-$CHROMEDRIVER_VERSION/chromedriver && \
    ln -fs /opt/chromedriver-$CHROMEDRIVER_VERSION/chromedriver /usr/local/bin/chromedriver

# Install Google Chrome
RUN curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list && \
    apt-get -yqq update && \
    apt-get -yqq install google-chrome-stable && \
    rm -rf /var/lib/apt/lists/*

# get websockisfy
RUN mkdir /websocksify
RUN wget https://github.com/novnc/websockify/archive/refs/tags/v0.10.0.tar.gz -O websockisfy.tar.gz
RUN tar -xvf websockisfy.tar.gz -C /websocksify --strip-components=1

# Add application
WORKDIR /taxi

ADD . .

RUN yarn install
RUN yarn build

RUN chmod 755 start.sh
RUN chmod 755 scripts/vnc.sh
RUN chmod 755 scripts/novnc.sh
RUN mkdir /recordings

EXPOSE 3000
EXPOSE 5900
EXPOSE 6081

RUN useradd -rm -d /home/chromey -s /bin/bash -g root -G sudo -u 1001 chromey
RUN chown chromey /recordings
RUN usermod -G staff chromey
USER chromey

ENV DISPLAY :1

CMD ["bash", "start.sh"]

