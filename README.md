# Taxi

## usage

```sh
# spin up taxi proxy 
docker build -t taxi .
docker run -p 8083:3000 -p 6081:6081 -v /tmp/recordings:/recordings --network test --hostname taxi.test --privileged taxi

# start gitlab-qa (needs patched to allow WEBDRIVER_HEADLESS passthrough)
QA_REMOTE_GRID=taxi.test:3000 WEBDRIVER_HEADLESS=0 CHROME_DISABLE_DEV_SHM=true exe/gitlab-qa Test::Integration::Integrations CE
 
# finished recording should be in /tmp/recordings on host.
```

### Notes:

This employs a very similar strategy as selenium grid 4, zalenium, etc. 

The original idea was to bake the taxi application into the QA container, which:
* wouldn't need to proxy commands
* could provide tools for remote debugging.

This implementation is non intrusive in that it doesn't require any changes to gitlab-qa or gitlab to work.
It is also lightweight and doesn't orchestrate any more chrome nodes (not a grid).

It is very much experimental and unpolished.